<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Tests\Unit;

use Exception;
use Ox\Components\Cache\Adapters\APCuAdapter;
use Ox\Components\Cache\Adapters\ArrayAdapter;
use Ox\Components\Cache\Adapters\NullAdapter;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Components\Cache\LayeredCache;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

class LayeredCacheTest extends TestCase
{
    private const CACHE_NAMESPACE = 'test_cache';

    private function forgekey(string $key): string
    {
        return self::CACHE_NAMESPACE . '-' . $key;
    }

    private function getAdapter(): CacheInterface
    {
        return $this->getMockBuilder(CacheInterface::class)->getMock();
    }

    private function getAdapterWithMethod(string $method, $return, int $count): CacheInterface
    {
        $adapter = $this->getAdapter();

        $adapter->expects($this->exactly($count))->method($method)->willReturn($return);

        return $adapter;
    }

    private function getAdapterWithGetMethod($return, int $get_count, int $set_count): CacheInterface
    {
        $adapter = $this->getAdapter();

        $adapter->expects($this->exactly($get_count))->method('get')->willReturn($return);
        $adapter->expects($this->exactly($set_count))->method('set')->willReturn(true);

        return $adapter;
    }

    private function getAdapterWithGetMultipleMethod($return, int $get_count, int $set_count): CacheInterface
    {
        $adapter = $this->getAdapter();

        $adapter->expects($this->exactly($get_count))->method('getMultiple')->willReturn($return);
        $adapter->expects($this->exactly($set_count))->method('setMultiple')->willReturn(true);

        return $adapter;
    }

    private function getAdapterWithSetMethod($return, int $count): CacheInterface
    {
        return $this->getAdapterWithMethod('set', $return, $count);
    }

    private function getAdapterWithDeleteMethod($return, int $count): CacheInterface
    {
        return $this->getAdapterWithMethod('delete', $return, $count);
    }

    private function getAdapterWithClearMethod($return, int $count): CacheInterface
    {
        return $this->getAdapterWithMethod('clear', $return, $count);
    }

    private function getAdapterWithHasMethod($return, int $count): CacheInterface
    {
        return $this->getAdapterWithMethod('has', $return, $count);
    }

    private function getAdapterWithSetMultipleMethod($return, int $count): CacheInterface
    {
        return $this->getAdapterWithMethod('setMultiple', $return, $count);
    }

    private function getAdapterWithDeleteMultipleMethod($return, int $count): CacheInterface
    {
        return $this->getAdapterWithMethod('deleteMultiple', $return, $count);
    }

    public function cacheLayerProvider(): array
    {
        return [
            'INNER'       => [LayeredCache::INNER],
            'OUTER'       => [LayeredCache::OUTER],
            'DISTR'       => [LayeredCache::DISTR],
            'INNER_OUTER' => [LayeredCache::INNER_OUTER],
            'INNER_DISTR' => [LayeredCache::INNER_DISTR],
        ];
    }

    /**
     * @throws CouldNotGetCache
     */
    public function testCacheMustBeInitialized(): void
    {
        $this->expectExceptionObject(CouldNotGetCache::hasNotBeenInitialized());

        LayeredCache::getCache(LayeredCache::INNER);
    }

    /**
     * @depends testCacheMustBeInitialized
     * @throws CouldNotGetCache
     */
    public function testCacheSingletonDoesShareSameReference(): void
    {
        $cache = LayeredCache::init('test_cache');

        $this->assertInstanceOf(LayeredCache::class, $cache);

        $same_cache = LayeredCache::getCache(LayeredCache::INNER_OUTER);
        $this->assertNotSame($cache, $same_cache);

        $other_cache = LayeredCache::getCache(LayeredCache::INNER_OUTER);
        $this->assertNotSame($cache, $other_cache);
        $this->assertNotSame($same_cache, $other_cache);
    }

    /**
     * @depends testCacheMustBeInitialized
     * @depends testCacheSingletonDoesShareSameReference
     *
     * @throws CouldNotGetCache
     */
    public function testCacheInstancesDoShareReferenceProperties(): void
    {
        $reflection = new ReflectionClass(LayeredCache::class);

        $to_share_properties = $this->getToShareProperties();

        $properties = [];
        foreach ($reflection->getProperties() as $_property) {
            if (!in_array($_property->getName(), $to_share_properties)) {
                continue;
            }

            $properties[] = $_property;
        }

        LayeredCache::init('test_cache')
                    ->setAdapter(LayeredCache::INNER_OUTER | LayeredCache::DISTR, new NullAdapter());

        $first_cache = LayeredCache::getCache(LayeredCache::INNER_OUTER)->setAdapter(
            LayeredCache::INNER,
            new ArrayAdapter(),
            ['is_namespaced' => true]
        );

        $second_cache = LayeredCache::getCache(LayeredCache::INNER_DISTR)->setAdapter(
            LayeredCache::INNER,
            new APCuAdapter(),
            ['is_namespaced' => false]
        );

        /** @var ReflectionProperty $_property */
        foreach ($properties as $_property) {
            if (!$_property->isPublic()) {
                $_property->setAccessible(true);
            }

            $_first_value  = $_property->getValue($first_cache);
            $_second_value = $_property->getValue($second_cache);

            $this->assertSame($_first_value, $_second_value);
        }
    }

    /**
     * @depends testCacheMustBeInitialized
     * @depends testCacheSingletonDoesShareSameReference
     *
     * @throws CouldNotGetCache
     */
    public function testCacheInstancesDoesNotShareSomeProperties(): void
    {
        $reflection = new ReflectionClass(LayeredCache::class);

        $to_not_share_properties = $this->getToNotShareProperties();

        $identical_properties = [
            'namespace',
            'namespace_delimiter',
        ];

        $properties = [];

        foreach ($reflection->getProperties() as $_property) {
            if (!in_array($_property->getName(), $to_not_share_properties)) {
                continue;
            }

            $properties[] = $_property;
        }

        LayeredCache::init('test_cache');

        $first_cache  = LayeredCache::getCache(LayeredCache::INNER_OUTER, 300);
        $second_cache = LayeredCache::getCache(LayeredCache::INNER_DISTR, 0)->withCompressor();

        /** @var ReflectionProperty $_property */
        foreach ($properties as $_property) {
            if ($_property->isStatic()) {
                continue;
            }

            if (!$_property->isPublic()) {
                $_property->setAccessible(true);
            }

            $_first_value  = $_property->getValue($first_cache);
            $_second_value = $_property->getValue($second_cache);

            if (in_array($_property->getName(), $identical_properties)) {
                $this->assertEquals($_first_value, $_second_value);
            } else {
                $this->assertNotEquals($_first_value, $_second_value);
            }
        }
    }

    /**
     * @depends testCacheMustBeInitialized
     * @depends testCacheInstancesDoShareReferenceProperties
     * @depends testCacheInstancesDoesNotShareSomeProperties
     */
    public function testAllNonStaticPropertiesSharingAreChecked(): void
    {
        $this->expectNotToPerformAssertions();

        $reflection = new ReflectionClass(LayeredCache::class);

        $to_check_properties = array_merge($this->getToShareProperties(), $this->getToNotShareProperties());

        foreach ($reflection->getProperties() as $_property) {
            if ($_property->isStatic()) {
                continue;
            }

            if (!in_array($_property->getName(), $to_check_properties)) {
                $this->fail(sprintf('Untested property sharing: %s', $_property->getName()));
            }
        }
    }

    /**
     * @depends      testCacheMustBeInitialized
     * @depends      testCacheSingletonDoesShareSameReference
     * @dataProvider cacheLayerProvider
     *
     * @param int $layer
     *
     * @throws CouldNotGetCache
     * @throws Exception
     */
    public function testCacheLayerIsSet(int $layer): void
    {
        $cache = LayeredCache::getCache($layer);

        $layer_set = $this->getPrivateProperty($cache, 'layer');
        $this->assertSame($layer, $layer_set);
    }

    public function layerCombinationMatrixProvider(): array
    {
        // Filter NONE layer
        $layers = array_filter(
            LayeredCache::LAYERS,
            function ($k) {
                return ($k !== 0);
            },
            ARRAY_FILTER_USE_KEY
        );

        $provider = [];

        // Max combination is 2^3 ([0-7])
        $max = (2 ** (count($layers))) - 1;

        // Generating provider labels for each possible combination
        foreach (range(1, $max) as $_layer) {
            $layer_provider_label = '';

            foreach ($layers as $_layer_int => $_layer_name) {
                if ($_layer & $_layer_int) {
                    $layer_provider_label .= "_{$_layer_name}";
                }
            }

            $layer_provider_label = ltrim($layer_provider_label, '_');

            foreach (range(0, $max) as $_i) {
                $provider_value_label = $layer_provider_label;

                $provider_value_label .= ($_i & 4) ? '_TRUE' : '_FALSE';
                $provider_value_label .= ($_i & 2) ? '_TRUE' : '_FALSE';
                $provider_value_label .= ($_i & 1) ? '_TRUE' : '_FALSE';

                $returns = [];

                $returns[] = (bool)($_i & 4);
                $returns[] = (bool)($_i & 2);
                $returns[] = (bool)($_i & 1);

                $provider[$provider_value_label] = [];
                array_push($provider[$provider_value_label], $_layer, ...$returns);
            }
        }

        return $provider;
    }

    /**
     * @depends      testCacheMustBeInitialized
     * @dataProvider layerCombinationMatrixProvider
     *
     * @param int $layer
     * @param     ...$returns
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function testGetLogic(int $layer, ...$returns): void
    {
        $adapters = $this->generateComplexDisjunctionAdapters('get', $layer, $returns);
        $expected = $this->getExpectedResultDisjunction($layer, $returns);
        $expected = ($expected) ? 'value' : 'default';

        $cache = $this->initCache($layer, $adapters, false);

        $actual = $cache->get('key', 'default');
        $this->assertEquals($expected, $actual);
    }

    /**
     * @depends      testCacheMustBeInitialized
     * @dataProvider layerCombinationMatrixProvider
     *
     * @param int $layer
     * @param     ...$returns
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function testSetLogic(int $layer, ...$returns): void
    {
        $adapters = $this->generateSimpleConjunctionAdapters('set', $layer, $returns);
        $expected = $this->getExpectedResultConjunction($layer, $returns);

        $cache = $this->initCache($layer, $adapters, false);

        $actual = $cache->set('key', 'value');
        $this->assertEquals($expected, $actual);
    }

    /**
     * @depends      testCacheMustBeInitialized
     * @dataProvider layerCombinationMatrixProvider
     *
     * @param int $layer
     * @param     ...$returns
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function testHasLogic(int $layer, ...$returns): void
    {
        $adapters = $this->generateSimpleDisjunctionAdapters('has', $layer, $returns);
        $expected = $this->getExpectedResultDisjunction($layer, $returns);

        $cache = $this->initCache($layer, $adapters, false);

        $actual = $cache->has('key');
        $this->assertEquals($expected, $actual);
    }

    /**
     * @depends      testCacheMustBeInitialized
     * @dataProvider layerCombinationMatrixProvider
     *
     * @param int $layer
     * @param     ...$returns
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function testDeleteLogic(int $layer, ...$returns): void
    {
        $adapters = $this->generateSimpleConjunctionAdapters('delete', $layer, $returns);
        $expected = $this->getExpectedResultConjunction($layer, $returns);

        $cache = $this->initCache($layer, $adapters, false);

        $actual = $cache->delete('key');
        $this->assertEquals($expected, $actual);
    }

    /**
     * @depends      testCacheMustBeInitialized
     * @dataProvider layerCombinationMatrixProvider
     *
     * @param int $layer
     * @param     ...$returns
     *
     * @throws CouldNotGetCache
     */
    public function testClearLogic(int $layer, ...$returns): void
    {
        $adapters = $this->generateSimpleConjunctionAdapters('clear', $layer, $returns);
        $expected = $this->getExpectedResultConjunction($layer, $returns);

        $cache = $this->initCache($layer, $adapters, false);

        $actual = $cache->clear();
        $this->assertEquals($expected, $actual);
    }

    /**
     * @depends      testCacheMustBeInitialized
     * @dataProvider layerCombinationMatrixProvider
     *
     * @param int $layer
     * @param     ...$returns
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function testGetMultipleLogic(int $layer, ...$returns): void
    {
        // Todo: Find why a fourth NULL value is here
        $returns = array_filter($returns, fn($v) => $v !== null);

        $adapters = $this->generateComplexDisjunctionAdapters('getMultiple', $layer, $returns);
        $expected = $this->getExpectedResultDisjunction($layer, $returns);
        $expected = $this->getGetMultipleExpectedReturn($expected);

        $cache = $this->initCache($layer, $adapters, false);

        $actual = $cache->getMultiple($this->getGetMultipleQuery(), 'default');
        $this->assertEquals($expected, $actual);
    }

    /**
     * @depends      testCacheMustBeInitialized
     * @dataProvider layerCombinationMatrixProvider
     *
     * @param int $layer
     * @param     ...$returns
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function testSetMultipleLogic(int $layer, ...$returns): void
    {
        $adapters = $this->generateSimpleConjunctionAdapters('setMultiple', $layer, $returns);
        $expected = $this->getExpectedResultConjunction($layer, $returns);

        $cache = $this->initCache($layer, $adapters, false);

        $actual = $cache->setMultiple(
            [
                'key1' => 'value1',
                'key2' => 'value2',
                'key3' => 'value3',
            ]
        );

        $this->assertEquals($expected, $actual);
    }

    /**
     * @depends      testCacheMustBeInitialized
     * @dataProvider layerCombinationMatrixProvider
     *
     * @param int $layer
     * @param     ...$returns
     *
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function testDeleteMultipleLogic(int $layer, ...$returns): void
    {
        $adapters = $this->generateSimpleConjunctionAdapters('deleteMultiple', $layer, $returns);
        $expected = $this->getExpectedResultConjunction($layer, $returns);

        $cache = $this->initCache($layer, $adapters, false);

        $actual = $cache->deleteMultiple(
            [
                'key1',
                'key2',
                'key3',
            ]
        );

        $this->assertEquals($expected, $actual);
    }

    /**
     * @depends testCacheMustBeInitialized
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function testGetMultipleAcrossLayers(): void
    {
        $adapters = $this->getMultipleAcrossLayersAdapters();
        $cache    = $this->initCache(LayeredCache::INNER_OUTER | LayeredCache::DISTR, $adapters, false);

        $expected = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
        ];

        $actual = $cache->getMultiple(
            [
                'key1',
                'key2',
                'key3',
            ]
        );

        $this->assertEquals($expected, $actual);
    }

    /**
     * @depends testCacheMustBeInitialized
     *
     * @return void
     * @throws CouldNotGetCache
     * @throws InvalidArgumentException
     */
    public function testDefaultTtlIsUsed(): void
    {
        $inner = $this->getAdapter();
        $outer = $this->getAdapter();
        $distr = $this->getAdapter();

        $default_ttl = 42;

        $inner
            ->expects($this->exactly(1))
            ->method('set')
            ->with('test_cache-key', 'value', $default_ttl)
            ->willReturn(true);

        $outer
            ->expects($this->exactly(1))
            ->method('set')
            ->with('test_cache-key', 'value', $default_ttl)
            ->willReturn(true);

        $distr
            ->expects($this->exactly(1))
            ->method('get')
            ->willReturn('value');

        $cache = $this->initCache(
            LayeredCache::INNER_OUTER | LayeredCache::DISTR,
            [$inner, $outer, $distr],
            false,
            $default_ttl
        );

        $cache->get('key');
    }

    /**
     * Compute the expected result when only one layer can return TRUE.
     *
     * @param int   $layer
     * @param array $returns
     *
     * @return bool
     */
    private function getExpectedResultDisjunction(int $layer, array $returns): bool
    {
        $expected = false;
        foreach ($returns as $_i => $_success) {
            $_layer = 2 ** $_i;

            if (($layer & $_layer) && $_success) {
                $expected = true;
            }
        }

        return $expected;
    }

    /**
     * Compute the expected command result when all the layers must return TRUE.
     *
     * @param int   $layer
     * @param array $returns
     *
     * @return bool
     */
    private function getExpectedResultConjunction(int $layer, array $returns): bool
    {
        $expected = null;
        foreach ($returns as $_i => $_success) {
            $_layer = 2 ** $_i;

            if (($layer & $_layer)) {
                if ($expected === null) {
                    $expected = $_success;
                } else {
                    $expected = $expected && $_success;
                }
            }
        }

        return (bool)$expected;
    }

    /**
     * Generate the appropriated adapters when the layers are called UNTIL some returns TRUE.
     *
     * @param string $type
     * @param int    $layer
     * @param array  $returns
     *
     * @return array
     */
    private function generateSimpleDisjunctionAdapters(string $type, int $layer, array $returns): array
    {
        $adapters = [];

        $previous_return   = false;
        $previously_called = null;

        foreach ($returns as $_i => $_success_or_not) {
            $_layer = 2 ** $_i;

            $count = 0;

            if ($layer & $_layer) {
                if (!$previous_return) {
                    $count++;

                    if ($previously_called === null) {
                        $previously_called = true;
                    }
                }

                if ($previous_return !== true) {
                    $previous_return = $_success_or_not;
                }
            }

            switch ($type) {
                case 'has':
                    $adapters[] = $this->getAdapterWithHasMethod($_success_or_not, $count);
                    break;

                default:
                    // None
            }
        }

        return $adapters;
    }

    /**
     * Generate the appropriated adapters when the layers are called UNTIL some returns TRUE.
     *
     * @param string $type
     * @param int    $layer
     * @param array  $returns
     *
     * @return array
     */
    private function generateComplexDisjunctionAdapters(string $type, int $layer, array $returns): array
    {
        $adapters = [];

        $previous_return   = false;
        $previously_called = null;

        $expected = $this->getExpectedResultDisjunction($layer, $returns);

        foreach ($returns as $_i => $_success) {
            $_layer = 2 ** $_i;

            $get_calls = 0;
            $set_calls = 0;

            if ($layer & $_layer) {
                if ($previous_return !== true) {
                    $get_calls++;

                    if ($previously_called === null) {
                        $previously_called = true;
                    }
                }

                if (($get_calls > 0) && !$_success && $previously_called && $expected) {
                    $set_calls++;
                }

                if ($previous_return !== true) {
                    $previous_return = $_success;
                }
            }

            switch ($type) {
                case 'get':
                    $adapters[] = $this->getAdapterWithGetMethod(($_success) ? 'value' : null, $get_calls, $set_calls);
                    break;

                case 'getMultiple':
                    $adapters[] = $this->getAdapterWithGetMultipleMethod(
                        $this->getGetMultipleAdapterReturn($_success),
                        $get_calls,
                        $set_calls
                    );
                    break;

                default:
                    // None
            }
        }

        return $adapters;
    }

    /**
     * Generate the appropriated adapters when all layers MUST be called.
     *
     * @param string $type
     * @param int    $layer
     * @param array  $returns
     *
     * @return array
     */
    private function generateSimpleConjunctionAdapters(string $type, int $layer, array $returns): array
    {
        $adapters = [];

        foreach ($returns as $_i => $_success_or_not) {
            $_layer = 2 ** $_i;

            $count = 0;

            if ($layer & $_layer) {
                $count++;
            }

            switch ($type) {
                case 'set':
                    $adapters[] = $this->getAdapterWithSetMethod($_success_or_not, $count);
                    break;

                case 'delete':
                    $adapters[] = $this->getAdapterWithDeleteMethod($_success_or_not, $count);
                    break;

                case 'clear':
                    $adapters[] = $this->getAdapterWithClearMethod($_success_or_not, $count);
                    break;

                case 'setMultiple':
                    $adapters[] = $this->getAdapterWithSetMultipleMethod($_success_or_not, $count);
                    break;

                case 'deleteMultiple':
                    $adapters[] = $this->getAdapterWithDeleteMultipleMethod($_success_or_not, $count);
                    break;

                default:
                    // None
            }
        }

        return $adapters;
    }

    private function getGetMultipleQuery(): array
    {
        return [
            'key1',
            'key2',
            'key3',
        ];
    }

    private function getGetMultipleAdapterReturn(bool $success): array
    {
        return [
            $this->forgeKey('key1') => ($success) ? 'value1' : null,
            $this->forgeKey('key2') => ($success) ? 'value2' : null,
            $this->forgeKey('key3') => ($success) ? 'value3' : null,
        ];
    }

    private function getGetMultipleExpectedReturn(bool $success): array
    {
        return [
            'key1' => ($success) ? 'value1' : 'default',
            'key2' => ($success) ? 'value2' : 'default',
            'key3' => ($success) ? 'value3' : 'default',
        ];
    }

    /**
     * Todo: Why setMultiple is not called on third layer?
     * @return CacheInterface[]
     */
    public function getMultipleAcrossLayersAdapters(): array
    {
        $inner = $this->getAdapterWithGetMultipleMethod(
            [
                $this->forgeKey('key1') => 'value1',
                $this->forgeKey('key2') => null,
                $this->forgeKey('key3') => null,
            ],
            1,
            1
        );

        $outer = $this->getAdapterWithGetMultipleMethod(
            [
                $this->forgeKey('key1') => null,
                $this->forgeKey('key2') => 'value2',
                $this->forgeKey('key3') => null,
            ],
            1,
            1
        );

        $distr = $this->getAdapterWithGetMultipleMethod(
            [
                $this->forgeKey('key1') => null,
                $this->forgeKey('key2') => null,
                $this->forgeKey('key3') => 'value3',
            ],
            1,
            0
        );

        return [$inner, $outer, $distr];
    }

    /**
     * @param mixed  $object   Classname or object (instance of the class) that contains the constant.
     * @param string $property Name of the property
     *
     * @return mixed The constant value
     * @throws Exception
     */
    private function getPrivateProperty($object, string $property)
    {
        // Obj
        if (!is_object($object)) {
            if (!class_exists($object)) {
                throw new Exception('The class does not exist ' . $object);
            }

            $object = new $object();
        }

        // Reflection
        try {
            $reflection = new \ReflectionClass($object);
            $property   = $reflection->getProperty($property);
        } catch (ReflectionException $e) {
            throw new Exception('The property does not exist ' . $e->getMessage());
        }

        // Accessibility
        if ($property->isPublic()) {
            throw new Exception('Property is already public');
        }

        $property->setAccessible(true);

        return $property->getValue($object);
    }

    /**
     * @param int      $layer
     * @param array    $adapters
     * @param bool     $layers_are_namespaced
     * @param int|null $default_ttl
     *
     * @return LayeredCache
     * @throws CouldNotGetCache
     */
    private function initCache(
        int   $layer,
        array $adapters,
        bool  $layers_are_namespaced = false,
        int   $default_ttl = 0
    ): LayeredCache {
        $cache = LayeredCache::init('test_cache');
        foreach ($adapters as $_i => $_adapter) {
            $cache->setAdapter(2 ** $_i, $_adapter, ['namespaced' => $layers_are_namespaced]);
        }

        return LayeredCache::getCache($layer, $default_ttl);
    }

    private function getToShareProperties(): array
    {
        return [
            'inner',
            'outer',
            'distr',
            'layer_metadata',
        ];
    }

    private function getToNotShareProperties(): array
    {
        return [
            'namespace',
            'namespace_delimiter',
            'layer',
            'compress',
            'default_ttl',
        ];
    }
}
