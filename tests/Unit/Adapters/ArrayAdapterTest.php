<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Tests\Unit\Adapters;

use Ox\Components\Cache\Adapters\AbstractCacheAdapter;
use Ox\Components\Cache\Adapters\ArrayAdapter;

/**
 * Class ArrayAdapterTest
 */
class ArrayAdapterTest extends AbstractAdapterTest
{
    public function getAdapter(): AbstractCacheAdapter
    {
        $cache = new ArrayAdapter();
        $cache->clear();

        return $cache;
    }
}
