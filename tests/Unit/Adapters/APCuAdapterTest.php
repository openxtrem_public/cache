<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Tests\Unit\Adapters;

use Ox\Components\Cache\Adapters\AbstractCacheAdapter;
use Ox\Components\Cache\Adapters\APCuAdapter;

/**
 * Class APCuAdapterTest
 */
class APCuAdapterTest extends AbstractAdapterTest
{
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        $cache = new APCuAdapter(self::$prefix);
        $cache->clear();
    }

    public function getAdapter(): AbstractCacheAdapter
    {
        $cache = new APCuAdapter(self::$prefix);
        $cache->clear();

        return $cache;
    }
}
