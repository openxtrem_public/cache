<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Tests\Unit\Adapters;

use Ox\Components\Cache\Adapters\AbstractCacheAdapter;
use Ox\Components\Cache\Adapters\FileAdapter;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;

/**
 * Class FileAdapterTest
 */
class FileAdapterTest extends AbstractAdapterTest
{
    private static $directory;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$directory = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid('file_cache_test', true);
    }

    /**
     * @throws CouldNotGetCache
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        $cache = new FileAdapter(self::$directory, self::$prefix);
        $cache->clear();
    }

    /**
     * @throws CouldNotGetCache
     */
    public function getAdapter(): AbstractCacheAdapter
    {
        $cache = new FileAdapter(self::$directory, self::$prefix);
        $cache->clear();

        return $cache;
    }
}
