<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Tests\Unit\Adapters;

use Ox\Components\Cache\Adapters\AbstractCacheAdapter;
use Ox\Components\Cache\Adapters\PredisAdapter;
use Predis\Client;
use Predis\Configuration\Options;
use Predis\Connection\Parameters;

class PredisAdapterTest extends AbstractAdapterTest
{
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        $parameters = new Parameters(
            [
                'scheme' => 'tcp',
                'host'   => getenv('REDIS_HOST') ?: 'redis',
                'port'   => 6379,
            ]
        );

        $options = new Options(
            [
                'exceptions' => true,
            ]
        );

        $cache = new PredisAdapter(new Client($parameters, $options));
        $cache->clear();
    }

    public function getAdapter(): AbstractCacheAdapter
    {
        $parameters = new Parameters(
            [
                'scheme' => 'tcp',
                'host'   => getenv('REDIS_HOST') ?: 'redis',
                'port'   => 6379,
            ]
        );

        $options = new Options(
            [
                'exceptions' => true,
            ]
        );

        $cache = new PredisAdapter(new Client($parameters, $options));
        $cache->clear();

        return $cache;
    }
}
