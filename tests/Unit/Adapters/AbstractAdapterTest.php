<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Tests\Unit\Adapters;

use Ox\Components\Cache\Adapters\AbstractCacheAdapter;
use Ox\Components\Cache\Exceptions\CouldNotUseKey;
use Ox\Components\Cache\Exceptions\InvalidKey;
use PHPUnit\Framework\TestCase;

/**
 * Todo: Test TTL.
 */
abstract class AbstractAdapterTest extends TestCase
{
    protected static $prefix = '';

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::$prefix = uniqid('test_cache', true);
    }

    protected function forgeKey(string $key): string
    {
        return static::$prefix . "-{$key}";
    }

    abstract public function getAdapter(): AbstractCacheAdapter;

    public function getProvider(): array
    {
        return [
            ['key1', 'default1', 'value1'],
        ];
    }

    public function setProvider(): array
    {
        return [
            ['key1', 'value1'],
        ];
    }

    public function deleteProvider(): array
    {
        return [
            ['key1'],
        ];
    }

    public function hasProvider(): array
    {
        return [
            ['key1', 'value1'],
        ];
    }

    public function getMultipleProvider(): array
    {
        return [
            [
                ['key1', 'key2', 'key3'],
                [
                    'key1' => 'value1',
                    'key2' => 'value2',
                    'key3' => 'value3',
                ],
            ],
        ];
    }

    public function setMultipleProvider(): array
    {
        return [
            [
                ['key1' => 'value1', 'key2' => 'value2', 'key3' => 'value3'],
            ],
        ];
    }

    public function deleteMultipleProvider(): array
    {
        return [
            [
                ['key1', 'key2', 'key3'],
            ],
        ];
    }

    /**
     * @see https://www.php-fig.org/psr/psr-16/
     */
    public function invalidKeysProvider(): array
    {
        $invalid_keys = [];

        foreach (str_split('{}()/\@:') as $_char) {
            $invalid_keys["Character: {$_char}"] = ["invalid{$_char}key", InvalidKey::class];
        }

//        $invalid_keys['integer']      = [10, CouldNotUseKey::class]; // No strict_mode yet
        $invalid_keys['bool']         = [false, CouldNotUseKey::class];
        $invalid_keys['empty string'] = ['', CouldNotUseKey::class];

        return $invalid_keys;
    }

    /**
     * @dataProvider getProvider
     */
    public function testGet($key, $default, $value): void
    {
        $key = $this->forgeKey($key);

        $cache = $this->getAdapter();

        $result = $cache->get($key);
        $this->assertNull($result);

        $result = $cache->get($key, $default);
        $this->assertEquals($default, $result);

        $result = $cache->get($key);
        $this->assertNull($result);

        $cache->set($key, $value);
        $result = $cache->get($key);
        $this->assertEquals($value, $result);
    }

    /**
     * @dataProvider setProvider
     */
    public function testSet($key, $value): void
    {
        $key = $this->forgeKey($key);

        $cache = $this->getAdapter();

        $result = $cache->set($key, $value);
        $this->assertTrue($result);
    }

    /**
     * @dataProvider deleteProvider
     */
    public function testDelete($key): void
    {
        $key = $this->forgeKey($key);

        $cache = $this->getAdapter();

        $result = $cache->delete($key);
        $this->assertTrue($result);
    }

    public function testClear(): void
    {
        $cache  = $this->getAdapter();
        $result = $cache->clear();
        $this->assertTrue($result);
    }

    /**
     * @dataProvider hasProvider
     */
    public function testHas($key, $value): void
    {
        $key = $this->forgeKey($key);

        $cache = $this->getAdapter();

        $result = $cache->has($key);
        $this->assertFalse($result);

        $cache->set($key, $value);
        $result = $cache->has($key);
        $this->assertTrue($result);
    }

    /**
     * @dataProvider getMultipleProvider
     */
    public function testGetMultiple($keys, $values): void
    {
        $prefixed_keys = [];
        foreach ($keys as $_key) {
            $prefixed_keys[] = $this->forgeKey($_key);
        }

        $cache = $this->getAdapter();

        $results = $cache->getMultiple($prefixed_keys);

        $i = 0;
        foreach ($results as $_key => $_value) {
            $i++;
            $this->assertEquals($this->forgeKey("key{$i}"), $_key);
            $this->assertNull($_value);
        }

        $prefixed_key_values = [];
        foreach ($values as $_key => $_value) {
            $prefixed_key_values[$this->forgeKey($_key)] = $_value;
        }

        $cache->setMultiple($prefixed_key_values);

        $results = $cache->getMultiple($prefixed_keys);

        $i = 0;
        foreach ($results as $_key => $_value) {
            $i++;

            $this->assertEquals($this->forgeKey("key{$i}"), $_key);
            $this->assertEquals("value{$i}", $_value);
        }
    }

    /**
     * @dataProvider setMultipleProvider
     */
    public function testSetMultiple($values): void
    {
        $prefixed_key_values = [];
        foreach ($values as $_key => $_value) {
            $prefixed_key_values[$this->forgeKey($_key)] = $_value;
        }

        $cache = $this->getAdapter();

        $result = $cache->setMultiple($prefixed_key_values);

        $this->assertTrue($result);
    }

    /**
     * @dataProvider deleteMultipleProvider
     */
    public function testDeleteMultiple($keys): void
    {
        $prefixed_keys = [];
        foreach ($keys as $_key) {
            $prefixed_keys[] = $this->forgeKey($_key);
        }

        $cache = $this->getAdapter();

        $result = $cache->deleteMultiple($prefixed_keys);

        $this->assertTrue($result);
    }

    /**
     * @dataProvider invalidKeysProvider
     */
    public function testGetMethodChecksInvalidKeys(mixed $key, string $exception_class): void
    {
        $cache = $this->getAdapter();

        $this->expectException($exception_class);

        $cache->get($key);
    }

    /**
     * @dataProvider invalidKeysProvider
     *
     * @param mixed $key
     */
    public function testSetMethodChecksInvalidKeys($key, string $exception_class): void
    {
        $cache = $this->getAdapter();

        $this->expectException($exception_class);

        $cache->set($key, null);
    }

    /**
     * @dataProvider invalidKeysProvider
     *
     * @param mixed $key
     */
    public function testDeleteMethodChecksInvalidKeys($key, string $exception_class): void
    {
        $cache = $this->getAdapter();

        $this->expectException($exception_class);

        $cache->delete($key);
    }

    /**
     * @dataProvider invalidKeysProvider
     *
     * @param mixed $key
     */
    public function testHasMethodChecksInvalidKeys($key, string $exception_class): void
    {
        $cache = $this->getAdapter();

        $this->expectException($exception_class);

        $cache->has($key);
    }

    /**
     * @dataProvider invalidKeysProvider
     *
     * @param mixed $key
     */
    public function testGetMultipleMethodChecksInvalidKeys($key, string $exception_class): void
    {
        $cache = $this->getAdapter();

        $this->expectException($exception_class);

        $cache->getMultiple([$key]);
    }

    /**
     * @dataProvider invalidKeysProvider
     *
     * @param mixed $key
     */
    public function testSetMultipleMethodChecksInvalidKeys($key, string $exception_class): void
    {
        $cache = $this->getAdapter();

        if (is_array($key) || is_object($key)) {
            $this->expectError();
        } else {
            $this->expectException($exception_class);
        }

        $cache->setMultiple([$key => null]);
    }

    /**
     * @dataProvider invalidKeysProvider
     *
     * @param mixed $key
     */
    public function testDeleteMultipleMethodChecksInvalidKeys($key, string $exception_class): void
    {
        $cache = $this->getAdapter();

        $this->expectException($exception_class);

        $cache->deleteMultiple([$key]);
    }
}
