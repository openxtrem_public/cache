<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Tests\Unit\Adapters;

use Ox\Components\Cache\Adapters\NullAdapter;
use PHPUnit\Framework\TestCase;

/**
 * Class NullAdapterTest
 */
class NullAdapterTest extends TestCase
{
    public function testGet(): void
    {
        $cache = new NullAdapter();

        $result = $cache->get('key');
        $this->assertNull($result);

        $result = $cache->get('key', 'default');
        $this->assertEquals('default', $result);

        $result = $cache->get('key');
        $this->assertNull($result);

        $cache->set('key', 'value');
        $result = $cache->get('key');
        $this->assertNull($result);
    }

    public function testSet(): void
    {
        $cache = new NullAdapter();

        $result = $cache->set('key', 'value');
        $this->assertTrue($result);
    }

    public function testDelete(): void
    {
        $cache = new NullAdapter();

        $result = $cache->delete('key');
        $this->assertTrue($result);
    }

    public function testClear(): void
    {
        $cache  = new NullAdapter();
        $result = $cache->clear();
        $this->assertTrue($result);
    }

    public function testHas(): void
    {
        $cache = new NullAdapter();

        $result = $cache->has('key');
        $this->assertFalse($result);

        $cache->set('key', 'value');
        $result = $cache->has('key');
        $this->assertFalse($result);
    }

    public function testGetMultiple(): void
    {
        $cache = new NullAdapter();

        $results = $cache->getMultiple(['key1', 'key2', 'key3']);

        $i = 0;
        foreach ($results as $_key => $_value) {
            $i++;
            $this->assertEquals("key{$i}", $_key);
            $this->assertNull($_value);
        }

        $cache->setMultiple(['key1' => 'value1', 'key2' => 'value2', 'key3' => 'value3']);

        $results = $cache->getMultiple(['key1', 'key2', 'key3']);

        $i = 0;
        foreach ($results as $_key => $_value) {
            $i++;

            $this->assertEquals("key{$i}", $_key);
            $this->assertNull($_value);
        }
    }

    public function testSetMultiple(): void
    {
        $cache = new NullAdapter();

        $result = $cache->setMultiple(['key1' => 'value1', 'key2' => 'value2', 'key3' => 'value3']);

        $this->assertTrue($result);
    }

    public function testDeleteMultiple(): void
    {
        $cache = new NullAdapter();

        $result = $cache->deleteMultiple(['key1', 'key2', 'key3']);

        $this->assertTrue($result);
    }
}
