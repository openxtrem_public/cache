<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache;

interface SearchableInterface
{
    /**
     * Return an iterable of keys according to given prefix.
     * Items in result MAY be expired.
     */
    public function list(?string $prefix = null): iterable;
}