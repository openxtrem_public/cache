<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Exceptions;

use Exception;
use Psr\SimpleCache\CacheException;

class InvalidArgument extends Exception implements CacheException
{
    /**
     * @return static
     */
    public static function mustBePositive(string $arg, int $count): self
    {
        return new static(sprintf("InvalidArgument-error-Provided parameter '%s' must be positive: %d", $arg, $count));
    }
}
