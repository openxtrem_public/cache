<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Exceptions;

use Exception;
use Psr\SimpleCache\CacheException;

class CouldNotGetCache extends Exception implements CacheException
{
    /**
     * @return static
     */
    public static function hasNotBeenInitialized(): self
    {
        return new static('CouldNotGetCache-error-Cache is not initialized');
    }

    /**
     * @return static
     */
    public static function cantCreateCacheDirectory(string $directory): self
    {
        return new static('CouldNotGetCache-error-Unable to create directory: %s', $directory);
    }
}
