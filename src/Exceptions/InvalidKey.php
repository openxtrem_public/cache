<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Exceptions;

use Exception;
use Psr\SimpleCache\InvalidArgumentException;

class InvalidKey extends Exception implements InvalidArgumentException
{
    /**
     * @return static
     */
    public static function hasInvalidCharacter(string $key): self
    {
        return new static(sprintf('InvalidKey-error-Provided key has invalid character: %s', $key));
    }
}
