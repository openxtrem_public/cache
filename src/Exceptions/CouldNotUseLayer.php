<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Exceptions;

use Exception;
use Psr\SimpleCache\InvalidArgumentException;

class CouldNotUseLayer extends Exception implements InvalidArgumentException
{
    /**
     * @return static
     */
    public static function hasNotBeenInitialized(): self
    {
        return new static('CouldNotUseLayer-error-Layer is not initialized');
    }
}
