<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Exceptions;

use Exception;
use Psr\SimpleCache\InvalidArgumentException;

class CouldNotUseKey extends Exception implements InvalidArgumentException
{
    /**
     * @return static
     */
    public static function isNotString(): self
    {
        return new static('CouldNotUseKey-error-Provided key is not a string');
    }

    /**
     * @return static
     */
    public static function hasInvalidCharacter(string $key): self
    {
        return new static(sprintf('CouldNotUseKey-error-Provided key has invalid character: %s', $key));
    }

    /**
     * @return static
     */
    public static function isEmpty(): self
    {
        return new static('CouldNotUseKey-error-Provided key is empty');
    }

    /**
     * @return static
     */
    public static function isNotIterable(): self
    {
        return new static('CouldNotUseKey-error-Provided keys are not iterable');
    }

    /**
     * @return static
     */
    public static function fileIsNotReadable(): self
    {
        return new static('CouldNotUseKey-error-File is not readable');
    }
}
