<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Adapters;

use DateInterval;
use Ox\Components\Cache\Exceptions\InvalidArgument;
use Ox\Components\Cache\SearchableInterface;
use Predis\ClientInterface;
use Throwable;

class PredisAdapter extends AbstractCacheAdapter implements SearchableInterface
{
    private ClientInterface $client;

    private int $scan_count = 10;

    public function __construct(ClientInterface $client, string $namespace = '', string $namespace_delimiter = '-')
    {
        parent::__construct($namespace, $namespace_delimiter);

        $this->client = $client;
    }

    /**
     * Set the COUNT workload for Redis SCAN command.
     * Default is 10.
     *
     * @throws InvalidArgument
     */
    public function setScanCount(int $count): void
    {
        if ($count < 1) {
            throw InvalidArgument::mustBePositive('count', $count);
        }

        $this->scan_count = $count;
    }

    /**
     * @inheritDoc
     */
    public function get(string $key, mixed $default = null): mixed
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        try {
            $value = $this->client->get($key);
        } catch (Throwable) {
            return $default;
        }

        if ($value === null) {
            return $default;
        }

        return unserialize($value);
    }

    /**
     * @inheritDoc
     */
    public function set(string $key, mixed $value, null|int|DateInterval $ttl = null): bool
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        try {
            $ttl = $this->convertTTLToSeconds($ttl);

            if ($ttl === 0) {
                $this->client->set($key, serialize($value));
            } else {
                $this->client->set($key, serialize($value), 'EX', $ttl);
            }
        } catch (Throwable) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function delete(string $key): bool
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        try {
            $this->client->del($key);
        } catch (Throwable) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        $prefix = '';
        if ($this->namespace !== '') {
            $prefix = $this->namespace . $this->namespace_delimiter;
        }

        $return = true;

        $offset = '0';

        try {
            do {
                $result = $this->client->scan($offset, ['MATCH' => "{$prefix}*", 'COUNT' => $this->scan_count]);
                $offset = $result[0];

                foreach ($result[1] as $_key) {
                    $return = $return && $this->delete($this->removeNamespaceFromKey($_key));
                }
            } while ($result[0] !== '0');
        } catch (Throwable) {
            return false;
        }

        return $return;
    }

    /**
     * @inheritDoc
     */
    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $this->checkIterableKeys($keys);

        if (!is_array($keys)) {
            $keys = iterator_to_array($keys);
        }

        $default_return = array_fill_keys($keys, $default);

        $return_keys = [];
        foreach ($keys as $_key) {
            $return_keys[] = $_key;
        }

        $keys = $this->namespaceKeys($keys);

        try {
            $values = $this->client->mget($keys);
        } catch (Throwable) {
            return $default_return;
        }

        $result = [];

        foreach ($values as $_i => $_value) {
            if ($_value === null) {
                $_value = $default;
            } else {
                $_value = unserialize($_value);
            }

            // Return of MGET command is indexed by keys order
            $result[$return_keys[$_i]] = $_value;
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function setMultiple(iterable $values, null|int|DateInterval $ttl = null): bool
    {
        $this->checkIterableKeyValues($values);

        if (!is_array($values)) {
            $values = iterator_to_array($values, true);
        }

        $ttl = $this->convertTTLToSeconds($ttl);

        $serialized_values = [];
        foreach ($values as $_key => $_value) {
            $serialized_values[$_key] = serialize($_value);
        }

        $namespaced_values = $this->namespaceIterable($serialized_values);

        try {
            $this->client->multi();

            $this->client->mset($namespaced_values);

            if ($ttl !== 0) {
                foreach ($namespaced_values as $_key => $_value) {
                    $this->client->expire($_key, $ttl);
                }
            }

            $this->client->exec();
        } catch (Throwable) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple(iterable $keys): bool
    {
        $this->checkIterableKeys($keys);

        if (!is_array($keys)) {
            $keys = iterator_to_array($keys);
        }

        $namespaced_keys = $this->namespaceKeys($keys);

        try {
            $this->client->del($namespaced_keys);
        } catch (Throwable) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function has(string $key): bool
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        try {
            return (bool)$this->client->exists($key);
        } catch (Throwable) {
            return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function list(?string $prefix = null): iterable
    {
        if ($prefix === null) {
            $prefix = '';
        }

        $prefix = $this->namespaceKey($prefix);

        $keys = [];

        $offset = '0';

        try {
            do {
                $result = $this->client->scan($offset, ['MATCH' => "{$prefix}*", 'COUNT' => $this->scan_count]);
                $offset = $result[0];

                foreach ($result[1] as $_key) {
                    $keys[] = $this->removeNamespaceFromKey($_key);
                }
            } while ($result[0] !== '0');
        } catch (Throwable) {
            return [];
        }

        return $keys;
    }
}
