<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Adapters;

use DateInterval;
use DateTimeImmutable;
use Ox\Components\Cache\Exceptions\CouldNotConvertTTL;
use Ox\Components\Cache\Traits\KeyCheckerTrait;
use Ox\Components\Cache\Traits\NamespaceAwareTrait;
use Psr\SimpleCache\CacheInterface;

abstract class AbstractCacheAdapter implements CacheInterface
{
    use NamespaceAwareTrait;
    use KeyCheckerTrait;

    public function __construct(string $namespace = '', string $namespace_delimiter = '-')
    {
        $this->namespace           = $this->sanitizeNamespace($namespace);
        $this->namespace_delimiter = $namespace_delimiter;
    }

    /**
     * @inheritDoc
     */
    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $this->checkIterableKeys($keys);

        $result = [];

        foreach ($keys as $_key) {
            $result[$_key] = $this->get($_key, $default);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function setMultiple(iterable $values, null|int|DateInterval $ttl = null): bool
    {
        $this->checkIterableKeyValues($values);

        $result = true;
        foreach ($values as $_key => $_value) {
            $result = ($this->set($_key, $_value, $ttl) && $result);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple(iterable $keys): bool
    {
        $this->checkIterableKeys($keys);

        $result = true;
        foreach ($keys as $_key) {
            $result = ($this->delete($_key) && $result);
        }

        return $result;
    }

    /**
     * @throws CouldNotConvertTTL
     */
    protected function convertTTLToSeconds(null|int|DateInterval $ttl = null): int
    {
        if ($ttl === null) {
            return 0;
        }

        if (is_integer($ttl)) {
            return $ttl;
        }

        if ($ttl instanceof DateInterval) {
            $reference_date = new DateTimeImmutable();
            $limit_date     = $reference_date->add($ttl);

            return ($limit_date->getTimestamp() - $reference_date->getTimestamp());
        }

        throw CouldNotConvertTTL::invalidTTLProvided();
    }
}
