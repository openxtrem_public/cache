<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Adapters;

use APCUIterator;
use DateInterval;
use Ox\Components\Cache\SearchableInterface;

class APCuAdapter extends AbstractCacheAdapter implements SearchableInterface
{
    /**
     * @inheritDoc
     */
    public function get(string $key, mixed $default = null): mixed
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        $success = false;
        $value   = apcu_fetch($key, $success);

        return ($success) ? $value : $default;
    }

    /**
     * @inheritDoc
     */
    public function set(string $key, mixed $value, null|int|DateInterval $ttl = null): bool
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        $store_result = apcu_store($key, $value, $this->convertTTLToSeconds($ttl));

        return is_array($store_result) ? empty($store_result) : $store_result;
    }

    /**
     * @inheritDoc
     */
    public function delete(string $key): bool
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        return (apcu_delete($key) || !apcu_exists($key));
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        if ($this->namespace !== '') {
            $iterator = new APCUIterator('/^' . preg_quote($this->namespace, '/') . '/i');

            $delete_result = apcu_delete($iterator);
            // If $delete_result is an array return true only if the array is empty.
            return is_array($delete_result) ? empty($delete_result) : $delete_result;
        }

        return apcu_clear_cache();
    }

    /**
     * @inheritDoc
     */
    public function has(string $key): bool
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        $exists_result = apcu_exists($key);
        return is_array($exists_result) ? empty($exists_result) : $exists_result;
    }

    /**
     * @inheritDoc
     */
    public function list(?string $prefix = null): iterable
    {
        $prefix  = preg_quote($prefix) . '.*';
        $key     = preg_quote($this->namespaceKey(''), '/');
        $pattern = $key . $prefix;

        $iterator = new APCUIterator('/^' . $pattern . '/i');

        foreach ($iterator as $_item) {
            yield $this->removeNamespaceFromKey($_item['key']);
        }
    }
}
