<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Adapters;

use DateInterval;
use FilesystemIterator;
use Iterator;
use Ox\Components\Cache\DirectoryFilterIterator;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Components\Cache\Exceptions\CouldNotUseKey;
use Ox\Components\Cache\SearchableInterface;
use RegexIterator;

class FileAdapter extends AbstractCacheAdapter implements SearchableInterface
{
    private string $directory;

    /**
     * @throws CouldNotGetCache
     */
    public function __construct(string $directory, string $namespace = '', string $namespace_delimiter = '-')
    {
        parent::__construct($namespace, $namespace_delimiter);

        // Todo: Not robust
        if (!is_dir($directory) && ($directory !== '/')) {
            if (!mkdir($directory, 0o755, true)) {
                throw CouldNotGetCache::cantCreateCacheDirectory($directory);
            }
        }

        $this->directory = $directory;
    }

    private function getFullPath(string $key): string
    {
        return $this->directory . DIRECTORY_SEPARATOR . $key;
    }

    /**
     * @inheritDoc
     */
    public function get(string $key, mixed $default = null): mixed
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        $path = $this->getFullPath($key);

        if (!file_exists($path)) {
            return $default;
        }

        if (!is_readable($path)) {
            throw CouldNotUseKey::fileIsNotReadable();
        }

        $lifetime = -1;

        $resource   = fopen($path, 'r');
        $first_line = fgets($resource);

        if ($first_line !== false) {
            $lifetime = intval($first_line);
        }

        if (($lifetime !== 0) && ($lifetime <= time())) {
            fclose($resource);

            unlink($path);

            return $default;
        }

        $data = '';
        while (($line = fgets($resource)) !== false) {
            $data .= $line;
        }

        fclose($resource);

        return unserialize($data);
    }

    /**
     * @inheritDoc
     */
    public function set(string $key, mixed $value, null|int|DateInterval $ttl = null): bool
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        $ttl = $this->convertTTLToSeconds($ttl);

        $lifetime = -1;

        if ($ttl === 0) {
            $lifetime = 0;
        } elseif ($ttl > 0) {
            $lifetime = time() + $ttl;
        }

        return (bool)file_put_contents(
            $this->getFullPath($key),
            $lifetime . PHP_EOL . serialize($value)
        );
    }

    /**
     * @inheritDoc
     */
    public function delete(string $key): bool
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        $path = $this->getFullPath($key);

        return (@unlink($path) || !file_exists($path));
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        $iterator = $this->getDirectoryIterator();

        foreach ($iterator as $_file_path) {
            @unlink($_file_path[0]);
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function has(string $key): bool
    {
        $this->checkKey($key);
        $key = $this->namespaceKey($key);

        $path = $this->getFullPath($key);

        $lifetime = -1;

        if (!is_file($path)) {
            return false;
        }

        $resource   = fopen($path, 'r');
        $first_line = fgets($resource);

        if ($first_line !== false) {
            $lifetime = intval($first_line);
        }

        fclose($resource);

        if (($lifetime !== 0) && ($lifetime <= time())) {
            unlink($path);

            return false;
        }

        return true;
    }

    public function list(?string $prefix = null): iterable
    {
        $regex = $this->getDirectoryIterator($prefix);

        foreach ($regex as $_filepath) {
            yield $_filepath['key'];
        }
    }

    private function getDirectoryIterator(?string $prefix = null): Iterator
    {
        $directory = new FilesystemIterator($this->directory, FilesystemIterator::SKIP_DOTS);
        $filter    = new DirectoryFilterIterator($directory);

        $prefix  = preg_quote($prefix ?? '') . '.*';
        $path    = preg_quote($this->getFullPath($this->namespaceKey('')), '/');
        $pattern = $path . "(?<key>{$prefix})";

        return new RegexIterator($filter, '/^' . $pattern . '/i', RegexIterator::GET_MATCH);
    }
}
