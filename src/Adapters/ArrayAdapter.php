<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Adapters;

use DateInterval;

class ArrayAdapter extends AbstractCacheAdapter
{
    private static array $data = [];

    /**
     * @inheritDoc
     */
    public function get(string $key, mixed $default = null): mixed
    {
        $this->checkKey($key);

        return (self::$data[$key]) ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function set(string $key, mixed $value, null|int|DateInterval $ttl = null): bool
    {
        $this->checkKey($key);

        $this->convertTTLToSeconds($ttl);

        self::$data[$key] = $value;

        return true;
    }

    /**
     * @inheritDoc
     */
    public function delete(string $key): bool
    {
        $this->checkKey($key);

        unset(self::$data[$key]);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        self::$data = [];

        return true;
    }

    /**
     * @inheritDoc
     */
    public function has(string $key): bool
    {
        $this->checkKey($key);

        return isset(self::$data[$key]);
    }

    public function __serialize(): array
    {
        return self::$data;
    }

    public function __unserialize(array $data): void
    {
        self::$data = $data;
    }
}
