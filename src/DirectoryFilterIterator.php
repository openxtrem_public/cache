<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache;

use FilesystemIterator;
use FilterIterator;

class DirectoryFilterIterator extends FilterIterator
{
    public function __construct(FilesystemIterator $iterator)
    {
        parent::__construct($iterator);
    }

    /**
     * @inheritdoc
     */
    public function accept(): bool
    {
        return !$this->getInnerIterator()->isDir();
    }
}
