<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Decorators;

use DateInterval;
use Ox\Components\Cache\SearchableInterface;
use Psr\SimpleCache\CacheInterface;

class CompressorDecorator implements CacheInterface, SearchableInterface
{
    private const COMPRESSION_TOKEN = '__gz__';

    private CacheInterface $cache;

    private bool $compress;

    public function __construct(CacheInterface $cache, bool $compress = true)
    {
        $this->cache = $cache;
        $this->enableCompressing($compress);
    }

    /**
     * @inheritDoc
     */
    public function get(string $key, mixed $default = null): mixed
    {
        $value = $this->cache->get($key, $default);

        return $this->unCompress($value);
    }

    /**
     * @inheritDoc
     */
    public function set(string $key, mixed $value, null|int|DateInterval $ttl = null): bool
    {
        return $this->cache->set($key, $this->compress($value), $ttl);
    }

    /**
     * @inheritDoc
     */
    public function delete(string $key): bool
    {
        return $this->cache->delete($key);
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        return $this->cache->clear();
    }

    /**
     * @inheritDoc
     */
    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $values = $this->cache->getMultiple($keys, $default);

        foreach ($values as $_key => &$_value) {
            $_value = $this->unCompress($_value);
        }

        return $values;
    }

    /**
     * @inheritDoc
     */
    public function setMultiple(iterable $values, null|int|DateInterval $ttl = null): bool
    {
        foreach ($values as $_key => &$_value) {
            $_value = $this->compress($_value);
        }

        return $this->cache->setMultiple($values, $ttl);
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple(iterable $keys): bool
    {
        return $this->cache->deleteMultiple($keys);
    }

    /**
     * @inheritDoc
     */
    public function has(string $key): bool
    {
        return $this->cache->has($key);
    }

    public function setAdapter(CacheInterface $cache): void
    {
        $this->cache = $cache;
    }

    public function enableCompressing(bool $compress): void
    {
        $this->compress = $compress;
    }

    private function compress(mixed $value): mixed
    {
        if ($this->compress) {
            return [
                self::COMPRESSION_TOKEN => gzcompress(serialize($value)),
            ];
        }

        return $value;
    }

    private function unCompress(mixed $value): mixed
    {
        if (is_array($value) && array_key_exists(self::COMPRESSION_TOKEN, $value)) {
            return unserialize(gzuncompress($value[self::COMPRESSION_TOKEN]));
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function list(?string $prefix = null): iterable
    {
        if ($this->cache instanceof SearchableInterface) {
            return $this->cache->list($prefix);
        }

        return [];
    }
}
