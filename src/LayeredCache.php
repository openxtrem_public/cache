<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache;

use DateInterval;
use Ox\Components\Cache\Decorators\CompressorDecorator;
use Ox\Components\Cache\Exceptions\CouldNotGetCache;
use Ox\Components\Cache\Exceptions\CouldNotUseLayer;
use Ox\Components\Cache\Traits\NamespaceAwareTrait;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

class LayeredCache implements CacheInterface, SearchableInterface
{
    use NamespaceAwareTrait;

    private const DEFAULT_TTL = 0;

    private const KEY_PREFIX = '-';

    public const NONE  = 0;
    public const INNER = 1;
    public const OUTER = 2;
    public const DISTR = 4;

    public const INNER_OUTER = self::INNER | self::OUTER;
    public const INNER_DISTR = self::INNER | self::DISTR;

    public const LAYERS = [
        self::NONE  => 'NONE',
        self::INNER => 'INNER',
        self::OUTER => 'OUTER',
        self::DISTR => 'DISTR',
    ];

    private int $default_ttl;

    private ?CompressorDecorator $inner = null;

    private ?CompressorDecorator $outer = null;

    private ?CompressorDecorator $distr = null;

    private ?int $layer = null;

    private LayerMetadata $layer_metadata;

    private bool $compress = false;

    private static ?LayeredCache $instance = null;

    private static array $hits = [];

    private static array $totals = [];

    private static int $total = 0;

    /**
     * @param int $default_ttl Default TTL when automatically setting a value
     *                         from a top-layer fetched item to a lower one.
     */
    private function __construct(
        string $namespace,
        string $namespace_delimiter = '-',
        int    $default_ttl = self::DEFAULT_TTL
    ) {
        $this->namespace           = $this->sanitizeNamespace($namespace);
        $this->namespace_delimiter = $namespace_delimiter;

        $this->default_ttl = $default_ttl;

        $this->layer_metadata = new LayerMetadata();

        foreach (self::LAYERS as $_layer => $_layer_name) {
            if ($_layer === 0) {
                continue;
            }

            $this->layer_metadata->set($_layer, ['namespaced' => false]);
        }
    }

    /**
     * @param int $default_ttl    Default TTL when automatically setting a value from a top-layer fetched item to a
     *                            lower one.
     *
     * @return static
     */
    public static function init(
        string $namespace,
        string $namespace_delimiter = '-',
        int    $default_ttl = self::DEFAULT_TTL
    ): self {
        return self::$instance = new self($namespace, $namespace_delimiter, $default_ttl);
    }

    public static function isInitialized(): bool
    {
        return (self::$instance instanceof self);
    }

    /**
     * @return $this
     */
    public function setAdapter(int $layer, CacheInterface $cache, array $metadata = []): self
    {
        if ($layer & self::INNER) {
            if ($this->inner instanceof CompressorDecorator) {
                $this->inner->setAdapter($cache);
            } else {
                $this->inner = new CompressorDecorator($cache, $this->compress);
            }

            $this->layer_metadata->set(self::INNER, $metadata);
        }

        if ($layer & self::OUTER) {
            if ($this->outer instanceof CompressorDecorator) {
                $this->outer->setAdapter($cache);
            } else {
                $this->outer = new CompressorDecorator($cache, $this->compress);
            }

            $this->layer_metadata->set(self::OUTER, $metadata);
        }

        if ($layer & self::DISTR) {
            if ($this->distr instanceof CompressorDecorator) {
                $this->distr->setAdapter($cache);
            } else {
                $this->distr = new CompressorDecorator($cache, $this->compress);
            }

            $this->layer_metadata->set(self::DISTR, $metadata);
        }

        return $this;
    }

    /**
     * @param int $default_ttl Default TTL when automatically setting a value from a top-layer fetched item to a lower
     *                         one.
     *
     * @throws CouldNotGetCache
     */
    public static function getCache(int $layer, int $default_ttl = self::DEFAULT_TTL): self
    {
        if (self::isInitialized()) {
            // Shareable data between two Caches must be an object in order to share references
            $cache              = clone self::$instance;
            $cache->default_ttl = $default_ttl;

            return $cache->setLayer($layer);
        }

        throw CouldNotGetCache::hasNotBeenInitialized();
    }

    /**
     * @return $this
     */
    private function setLayer(int $layer): self
    {
        $this->layer = $layer;

        return $this;
    }

    public function withCompressor(): self
    {
        $cache           = clone $this;
        $cache->compress = true;

        return $cache;
    }

    /**
     *
     * @throws CouldNotUseLayer
     */
    private function getAdapter(int $layer): CacheInterface
    {
        if (($this->inner !== null) && ($layer & self::INNER)) {
            $this->inner->enableCompressing($this->compress);

            return $this->inner;
        }

        if (($this->outer !== null) && ($layer & self::OUTER)) {
            $this->outer->enableCompressing($this->compress);

            return $this->outer;
        }

        if (($this->distr !== null) && ($layer & self::DISTR)) {
            $this->distr->enableCompressing($this->compress);

            return $this->distr;
        }

        throw CouldNotUseLayer::hasNotBeenInitialized();
    }

    private function hit(int $layer, string $key): void
    {
        [$prefix, $subkey] = $this->extractPrefix($key);

        $layer = self::LAYERS[$layer];

        if (!isset(self::$hits[$prefix][$subkey][$layer])) {
            self::$hits[$prefix][$subkey] = array_fill_keys(self::LAYERS, 0);
        }

        if (!isset(self::$totals[$prefix][$layer])) {
            self::$totals[$prefix] = array_fill_keys(self::LAYERS, 0);
        }

        self::$hits[$prefix][$subkey][$layer]++;
        self::$totals[$prefix][$layer]++;
        self::$total++;
    }

    private function extractPrefix(string $key): array
    {
        $key = $this->removeNamespaceFromKey($key);

        $pos = strpos($key, self::KEY_PREFIX, 0);

        if ($pos === false) {
            return [$key, ''];
        }

        return [
            substr($key, 0, $pos), // $pos instead of $pos + 1 because we remove KEY_PREFIX
            substr($key, $pos + 1), // $pos + 1 instead of $pos because we remove KEY_PREFIX
        ];
    }

    private function isLayerNamespaced(int $layer): bool
    {
        return $this->layer_metadata->get($layer, 'namespaced');
    }

    public function getMetadata(): LayerMetadata
    {
        return clone $this->layer_metadata;
    }

    private function hits(string $layer, array $keys): void
    {
        foreach ($keys as $_key) {
            $this->hit($layer, $_key);
        }
    }

    public static function getHits(): array
    {
        return self::$hits;
    }

    public static function getTotals(): array
    {
        uasort(self::$totals, fn($a, $b) => (array_sum($b) <=> array_sum($a)));

        return self::$totals;
    }

    public static function getTotal(): int
    {
        return self::$total;
    }

    /**
     * @inheritDoc
     */
    public function get(string $key, mixed $default = null, null|int|DateInterval $ttl = null): mixed
    {
        $namespaced_key = $this->namespaceKey($key);

        $ttl = ($ttl !== null) ? $ttl : $this->default_ttl;

        if ($this->inner && ($this->layer & self::INNER)) {
            // Default value is returned at the end of function
            $value = $this->getForLayer(self::INNER, $namespaced_key, $key, null);

            if ($value !== null) {
                $this->hit(self::INNER, $key);

                return $value;
            }
        }

        if ($this->outer && ($this->layer & self::OUTER)) {
            // Default value is returned at the end of function
            $value = $this->getForLayer(self::OUTER, $namespaced_key, $key, null);

            if ($value !== null) {
                $this->hit(self::OUTER, $key);

                if ($this->inner && ($this->layer & self::INNER)) {
                    $this->setForLayer(self::INNER, $namespaced_key, $key, $value, $ttl);
                }

                return $value;
            }
        }

        if ($this->distr && ($this->layer & self::DISTR)) {
            // Default value is returned at the end of function
            $value = $this->getForLayer(self::DISTR, $namespaced_key, $key, null);

            if ($value !== null) {
                $this->hit(self::DISTR, $key);

                if ($this->outer && ($this->layer & self::OUTER)) {
                    $this->setForLayer(self::OUTER, $namespaced_key, $key, $value, $ttl);
                }

                if ($this->inner && ($this->layer & self::INNER)) {
                    $this->setForLayer(self::INNER, $namespaced_key, $key, $value, $ttl);
                }

                return $value;
            }
        }

        $this->hit(self::NONE, $key);

        return $default;
    }

    /**
     * @inheritDoc
     */
    public function set(string $key, mixed $value, null|int|DateInterval $ttl = null): bool
    {
        $inner = $outer = $distr = true;

        $namespaced_key = $this->namespaceKey($key);

        if ($this->inner && ($this->layer & self::INNER)) {
            $inner = $this->setForLayer(self::INNER, $namespaced_key, $key, $value, $ttl);
        }

        if ($this->outer && ($this->layer & self::OUTER)) {
            $outer = $this->setForLayer(self::OUTER, $namespaced_key, $key, $value, $ttl);
        }

        if ($this->distr && ($this->layer & self::DISTR)) {
            $distr = $this->setForLayer(self::DISTR, $namespaced_key, $key, $value, $ttl);
        }

        return ($inner && $outer && $distr);
    }

    /**
     * @inheritDoc
     */
    public function delete(string $key): bool
    {
        $namespaced_key = $this->namespaceKey($key);
        $inner          = $outer = $distr = true;

        if ($this->inner && ($this->layer & self::INNER)) {
            $inner = $this->deleteForLayer(self::INNER, $namespaced_key, $key);
        }

        if ($this->outer && ($this->layer & self::OUTER)) {
            $outer = $this->deleteForLayer(self::OUTER, $namespaced_key, $key);
        }

        if ($this->distr && ($this->layer & self::DISTR)) {
            $distr = $this->deleteForLayer(self::DISTR, $namespaced_key, $key);
        }

        return ($inner && $outer && $distr);
    }

    /**
     * @inheritDoc
     */
    public function clear(): bool
    {
        $inner = $outer = $distr = true;

        if ($this->inner && ($this->layer & self::INNER)) {
            $inner = $this->inner->clear();
        }

        if ($this->outer && ($this->layer & self::OUTER)) {
            $outer = $this->outer->clear();
        }

        if ($this->distr && ($this->layer & self::DISTR)) {
            $distr = $this->distr->clear();
        }

        return ($inner && $outer && $distr);
    }

    /**
     * @inheritDoc
     */
    public function getMultiple(iterable $keys, mixed $default = null, null|int|DateInterval $ttl = null): iterable
    {
        $inner_values = $outer_values = $distr_values = [];

        $ttl = ($ttl !== null) ? $ttl : $this->default_ttl;

        // If not an array, values are a Traversable
        if (!is_array($keys)) {
            $keys = iterator_to_array($keys);
        }

        $used_keys = $keys;

        if ($this->inner && ($this->layer & self::INNER)) {
            [$inner_values, $null_keys] = $this->getMultipleValuesWithoutNS(self::INNER, $used_keys);

            if (!$null_keys) {
                return $inner_values;
            }

            $used_keys = $null_keys;
        }

        if ($this->outer && ($this->layer & self::OUTER)) {
            [$outer_values, $null_keys] = $this->getMultipleValuesWithoutNS(self::OUTER, $used_keys);

            if (!$null_keys) {
                $values = array_merge($inner_values, $outer_values);

                if ($this->inner && ($this->layer & self::INNER)) {
                    $namespaced_values = $this->namespaceIterable($values);

                    $this->setMultipleForLayer(self::INNER, $namespaced_values, $values, $ttl);
                }

                return $values;
            }

            $used_keys = $null_keys;
        }

        if ($this->distr && ($this->layer & self::DISTR)) {
            [$distr_values, $null_keys] = $this->getMultipleValuesWithoutNS(self::DISTR, $used_keys);

            if (!$null_keys) {
                $values = array_merge($inner_values, $outer_values, $distr_values);

                if ($this->outer && ($this->layer & self::OUTER)) {
                    $namespaced_values = $this->namespaceIterable($values);

                    $this->setMultipleForLayer(self::OUTER, $namespaced_values, $values, $ttl);
                }

                if ($this->inner && ($this->layer & self::INNER)) {
                    $namespaced_values = $this->namespaceIterable($values);

                    $this->setMultipleForLayer(self::INNER, $namespaced_values, $values, $ttl);
                }

                return $values;
            }

            $used_keys = $null_keys;
        }

        $this->hits(self::NONE, $used_keys);

        $default_values = array_fill_keys($used_keys, $default);

        return array_merge($inner_values, $outer_values, $distr_values, $default_values);
    }

    /**
     * @throws InvalidArgumentException
     */
    private function getMultipleValuesWithoutNS(int $layer, array $keys): array
    {
        $used_keys = $keys;

        if (!$this->isLayerNamespaced($layer)) {
            $used_keys = $this->namespaceKeys($keys);
        }

        [$layer_values, $null_keys] = $this->getMultipleValues($used_keys, $layer);

        if (!$this->isLayerNamespaced($layer)) {
            $layer_values = $this->removeNamespaceFromKeyValues($layer_values);
            $null_keys    = $this->removeNamespaceFromKeys($null_keys);
        }

        return [$layer_values, $null_keys];
    }

    /**
     * @inheritDoc
     */
    public function setMultiple(iterable $values, null|int|DateInterval $ttl = null): bool
    {
        $inner = $outer = $distr = true;

        // If not an array, values are a Traversable
        if (!is_array($values)) {
            $values = iterator_to_array($values, true);
        }

        $namespaced_values = $this->namespaceIterable($values);

        if (!is_array($values)) {
            $values = iterator_to_array($values);
        }

        if ($this->inner && ($this->layer & self::INNER)) {
            $inner = $this->setMultipleForLayer(self::INNER, $namespaced_values, $values, $ttl);
        }

        if ($this->outer && ($this->layer & self::OUTER)) {
            $outer = $this->setMultipleForLayer(self::OUTER, $namespaced_values, $values, $ttl);
        }

        if ($this->distr && ($this->layer & self::DISTR)) {
            $distr = $this->setMultipleForLayer(self::DISTR, $namespaced_values, $values, $ttl);
        }

        return ($inner && $outer && $distr);
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple(iterable $keys): bool
    {
        $inner = $outer = $distr = true;

        if (!is_array($keys)) {
            $keys = iterator_to_array($keys);
        }

        $namespaced_keys = $this->namespaceKeys($keys);

        if ($this->inner && ($this->layer & self::INNER)) {
            $inner = $this->deleteMultipleForLayer(self::INNER, $namespaced_keys, $keys);
        }

        if ($this->outer && ($this->layer & self::OUTER)) {
            $outer = $this->deleteMultipleForLayer(self::OUTER, $namespaced_keys, $keys);
        }

        if ($this->distr && ($this->layer & self::DISTR)) {
            $distr = $this->deleteMultipleForLayer(self::DISTR, $namespaced_keys, $keys);
        }

        return ($inner && $outer && $distr);
    }

    /**
     * @inheritDoc
     */
    public function has(string $key): bool
    {
        $namespaced_key = $this->namespaceKey($key);

        if ($this->inner && ($this->layer & self::INNER)) {
            if ($this->hasForLayer(self::INNER, $namespaced_key, $key)) {
                return true;
            }
        }

        if ($this->outer && ($this->layer & self::OUTER)) {
            if ($this->hasForLayer(self::OUTER, $namespaced_key, $key)) {
                return true;
            }
        }

        if ($this->distr && ($this->layer & self::DISTR)) {
            if ($this->hasForLayer(self::DISTR, $namespaced_key, $key)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @throws InvalidArgumentException
     */
    private function getMultipleValues(array $keys, int $layer): array
    {
        // Default value is returned at the end of function
        $cache_values = $this->getAdapter($layer)->getMultiple($keys, null);

        // If not an array, values are a Traversable
        if (!is_array($cache_values)) {
            $cache_values = iterator_to_array($cache_values);
        }

        // array_filter preserve the keys
        $non_null_values = array_filter($cache_values, fn($value) => ($value !== null));

        // Keys not retrieved in layer
        $null_keys = array_diff($keys, array_keys($non_null_values));

        // Keys retrieved in layer
        if ($valued_keys = array_diff($keys, $null_keys)) {
            $this->hits($layer, $valued_keys);
        }

        return [
            $non_null_values,
            $null_keys,
        ];
    }

    /**
     * @throws InvalidArgumentException
     */
    private function getForLayer(int $layer, string $namespaced_key, string $key, mixed $default): mixed
    {
        $used_key = $key;

        if ($layer & self::INNER) {
            if (!$this->isLayerNamespaced(self::INNER)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::INNER)->get($used_key, $default);
        }

        if ($layer & self::OUTER) {
            if (!$this->isLayerNamespaced(self::OUTER)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::OUTER)->get($used_key, $default);
        }

        if ($layer & self::DISTR) {
            if (!$this->isLayerNamespaced(self::DISTR)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::DISTR)->get($used_key, $default);
        }

        return $default;
    }

    /**
     * @throws InvalidArgumentException
     */
    private function setForLayer(
        int                   $layer,
        string                $namespaced_key,
        string                $key,
        mixed                 $value,
        null|int|DateInterval $ttl
    ): bool {
        $used_key = $key;

        if ($layer & self::INNER) {
            if (!$this->isLayerNamespaced(self::INNER)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::INNER)->set($used_key, $value, $ttl);
        }


        if ($layer & self::OUTER) {
            if (!$this->isLayerNamespaced(self::OUTER)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::OUTER)->set($used_key, $value, $ttl);
        }


        if ($layer & self::DISTR) {
            if (!$this->isLayerNamespaced(self::DISTR)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::DISTR)->set($used_key, $value, $ttl);
        }

        return true;
    }

    /**
     * @throws InvalidArgumentException
     */
    private function deleteForLayer(int $layer, string $namespaced_key, string $key): bool
    {
        $used_key = $key;

        if ($layer & self::INNER) {
            if (!$this->isLayerNamespaced(self::INNER)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::INNER)->delete($used_key);
        }

        if ($layer & self::OUTER) {
            if (!$this->isLayerNamespaced(self::OUTER)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::OUTER)->delete($used_key);
        }

        if ($layer & self::DISTR) {
            if (!$this->isLayerNamespaced(self::DISTR)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::DISTR)->delete($used_key);
        }

        return true;
    }

    /**
     * @throws InvalidArgumentException
     */
    private function hasForLayer(int $layer, string $namespaced_key, string $key): bool
    {
        $used_key = $key;

        if ($layer & self::INNER) {
            if (!$this->isLayerNamespaced(self::INNER)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::INNER)->has($used_key);
        }

        if ($layer & self::OUTER) {
            if (!$this->isLayerNamespaced(self::OUTER)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::OUTER)->has($used_key);
        }

        if ($layer & self::DISTR) {
            if (!$this->isLayerNamespaced(self::DISTR)) {
                $used_key = $namespaced_key;
            }

            return $this->getAdapter(self::DISTR)->has($used_key);
        }

        return false;
    }

    /**
     * @throws InvalidArgumentException
     */
    private function setMultipleForLayer(
        int                   $layer,
        array                 &$namespaced_values,
        array                 &$values,
        null|int|DateInterval $ttl
    ): bool {
        $used_values = $values;

        if ($layer & self::INNER) {
            if (!$this->isLayerNamespaced(self::INNER)) {
                $used_values = $namespaced_values;
            }

            return $this->getAdapter(self::INNER)->setMultiple($used_values, $ttl);
        }


        if ($layer & self::OUTER) {
            if (!$this->isLayerNamespaced(self::OUTER)) {
                $used_values = $namespaced_values;
            }

            return $this->getAdapter(self::OUTER)->setMultiple($used_values, $ttl);
        }


        if ($layer & self::DISTR) {
            if (!$this->isLayerNamespaced(self::DISTR)) {
                $used_values = $namespaced_values;
            }

            return $this->getAdapter(self::DISTR)->setMultiple($used_values, $ttl);
        }

        return true;
    }

    /**
     * @throws InvalidArgumentException
     */
    private function deleteMultipleForLayer(int $layer, array $namespaced_keys, array $keys): bool
    {
        $used_keys = $keys;

        if ($layer & self::INNER) {
            if (!$this->isLayerNamespaced(self::INNER)) {
                $used_keys = $namespaced_keys;
            }

            return $this->getAdapter(self::INNER)->deleteMultiple($used_keys);
        }


        if ($layer & self::OUTER) {
            if (!$this->isLayerNamespaced(self::OUTER)) {
                $used_keys = $namespaced_keys;
            }

            return $this->getAdapter(self::OUTER)->deleteMultiple($used_keys);
        }


        if ($layer & self::DISTR) {
            if (!$this->isLayerNamespaced(self::DISTR)) {
                $used_keys = $namespaced_keys;
            }

            return $this->getAdapter(self::DISTR)->deleteMultiple($used_keys);
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function list(?string $prefix = null): iterable
    {
        if ($prefix === null) {
            $prefix = '';
        }

        if (($this->inner instanceof SearchableInterface) && ($this->layer & self::INNER)) {
            if (!$this->isLayerNamespaced(self::INNER)) {
                $prefix = $this->namespaceKey($prefix);
            }

            if ($this->isLayerNamespaced(self::INNER)) {
                return $this->inner->list($prefix);
            }

            foreach ($this->inner->list($prefix) as $_key) {
                yield $this->removeNamespaceFromKey($_key);
            }
        }

        if (($this->outer instanceof SearchableInterface) && ($this->layer & self::OUTER)) {
            if (!$this->isLayerNamespaced(self::OUTER)) {
                $prefix = $this->namespaceKey($prefix);
            }

            if ($this->isLayerNamespaced(self::OUTER)) {
                return $this->outer->list($prefix);
            }

            foreach ($this->outer->list($prefix) as $_key) {
                yield $this->removeNamespaceFromKey($_key);
            }
        }

        if (($this->distr instanceof SearchableInterface) && ($this->layer & self::DISTR)) {
            if (!$this->isLayerNamespaced(self::DISTR)) {
                $prefix = $this->namespaceKey($prefix);
            }

            if ($this->isLayerNamespaced(self::DISTR)) {
                return $this->distr->list($prefix);
            }

            foreach ($this->distr->list($prefix) as $_key) {
                yield $this->removeNamespaceFromKey($_key);
            }
        }

        return [];
    }
}
