<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache;

class LayerMetadata
{
    private array $data = [];

    public function get(int $layer, string $name): mixed
    {
        return $this->data[$layer][$name] ?? null;
    }

    public function set(int $layer, array $data): void
    {
        $this->data[$layer] = $data;
    }
}
