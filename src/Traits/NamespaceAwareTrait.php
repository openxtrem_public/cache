<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Traits;

trait NamespaceAwareTrait
{
    protected string $namespace;

    protected string $namespace_delimiter;

    protected function sanitizeNamespace(string $namespace): string
    {
        return preg_replace('/[^\w]+/', '_', $namespace);
    }

    protected function namespaceKey(string $key): string
    {
        if ($this->namespace === '') {
            return $key;
        }

        return $this->namespace . $this->namespace_delimiter . $key;
    }

    protected function namespaceKeys(array $keys): array
    {
        if ($this->namespace === '') {
            return $keys;
        }

        $namespaced_keys = [];
        foreach ($keys as $_key) {
            $namespaced_keys[] = $this->namespaceKey($_key);
        }

        return $namespaced_keys;
    }

    protected function namespaceIterable(array &$values): array
    {
        $namespaced_values = [];
        foreach ($values as $_key => &$_value) {
            $namespaced_values[$this->namespaceKey($_key)] = $_value;
        }

        return $namespaced_values;
    }

    protected function removeNamespaceFromKey(string $key): string
    {
        if ($this->namespace === '') {
            return $key;
        }

        if (str_starts_with($key, $this->namespace . $this->namespace_delimiter)) {
            return substr($key, strlen($this->namespace . $this->namespace_delimiter));
        }

        return $key;
    }

    protected function removeNamespaceFromKeys(array $keys): array
    {
        if ($this->namespace === '') {
            return $keys;
        }

        $un_namespaced_keys = [];

        foreach ($keys as $_key) {
            $un_namespaced_keys[] = $this->removeNamespaceFromKey($_key);
        }

        return $un_namespaced_keys;
    }

    protected function removeNamespaceFromKeyValues(array &$values): array
    {
        if ($this->namespace === '') {
            return $values;
        }

        $un_namespaced_values = [];

        foreach ($values as $_key => &$_value) {
            $un_namespaced_values[$this->removeNamespaceFromKey($_key)] = $_value;
        }

        return $un_namespaced_values;
    }
}
