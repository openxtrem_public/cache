<?php

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\Cache\Traits;

use Ox\Components\Cache\Exceptions\CouldNotUseKey;
use Ox\Components\Cache\Exceptions\InvalidKey;

trait KeyCheckerTrait
{
    private const RESERVED_CHARACTERS = '[{}()/\@:]';

    /**
     * @throws CouldNotUseKey
     * @throws InvalidKey
     */
    protected function checkKey(mixed $key): void
    {
        if (!is_string($key)) {
            throw CouldNotUseKey::isNotString();
        }

        if (strpbrk($key, self::RESERVED_CHARACTERS) !== false) {
            throw InvalidKey::hasInvalidCharacter($key);
        }

        if ($key === '') {
            throw CouldNotUseKey::isEmpty();
        }
    }

    /**
     * @throws CouldNotUseKey
     * @throws InvalidKey
     */
    protected function checkIterableKeys(iterable $keys): void
    {
        foreach ($keys as $_key) {
            $this->checkKey($_key);
        }
    }

    /**
     * @throws CouldNotUseKey
     * @throws InvalidKey
     */
    protected function checkIterableKeyValues(iterable $keys): void
    {
        foreach ($keys as $_key => &$_value) {
            $this->checkKey($_key);
        }
    }
}